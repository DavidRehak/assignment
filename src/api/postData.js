import axios from "axios";
import {fetchBoardsData, fetchListsData} from "./fetchData";
import {getFirebaseApiUrl} from "../utils/firebase";

export const postData = (endpoint, payload, ids = {}) => {
    axios.post(getFirebaseApiUrl() + endpoint, payload)
        .then(() => {
            if (ids.boardId) {
                return fetchListsData(ids.boardId);
            } else {
                return fetchBoardsData();
            }
        });
}