import axios from "axios";
import {ref} from "vue";
import {getFirebaseApiUrl} from "../utils/firebase";

const boards = ref();
const lists = ref();

const fetchBoardsData = async () => {
    await axios.get(`${getFirebaseApiUrl()}/boards.json`)
        .then(res => boards.value = res.data)
        .catch(err => console.log(err));
}

const fetchListsData = async (boardId) => {
    await axios.get(`${getFirebaseApiUrl()}/boards/${boardId}/lists.json`)
        .then(res => lists.value = res.data)
        .catch(err => console.log(err));
}

export {boards, lists, fetchBoardsData, fetchListsData}